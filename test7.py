import unittest
from hw7 import *


class TestHomework7(unittest.TestCase):

    def setUp(self):
        pass

    # music_recs
    def test_music_recs(self):
        a7x = ['Five Finger Death Punch', 'Bullet for My Valentine',
               'All That Remains', 'Killswitch Engage', 'Stone Sour',
               'Rise Against', 'Three Days Grace', 'Sick Puppies',
               'In This Moment', 'Mudvayne']
        atl = ['Mayday Parade', 'The Maine', 'We the Kings', 'Pierce the Veil',
               'Forever the Sickest Kids', 'Boys Like Girls',
               'Sleeping with Sirens', 'We Are the In Crowd', 'Go Radio',
               'Breathe Carolina']
        self.assertEqual(music_recs('Avenged Sevenfold'), a7x)
        self.assertEqual(music_recs('All Time Low'), atl)
        with self.assertRaises(Exception, msg='Artist not found'):
            music_recs('Eric Kutschera')

    # xkcd
    def test_xkcd(self):
        with open('test_imgs/interdisciplinary.png', 'rb') as f_name:
            image = f_name.read()
        comic_title = 'Interdisciplinary'
        title_text = "Replace the pendulums with history students and you'll qualify for a grant!"
        err_msg = 'comic_num is an invalid xkcd comic. try [1-1490]'  # number likely out of date
        computed = xkcd(755)
        self.assertEqual(computed['comic_title'], comic_title)
        self.assertEqual(computed['title_text'], title_text)
        with open(computed['image'], 'rb') as f_name:
            computed_img = f_name.read()
        self.assertEqual(computed_img, image)
        with self.assertRaises(ValueError, msg=err_msg):
            xkcd(2000)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
