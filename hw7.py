""" Homework 7
-- Due Sunday, Mar. 15th at 23:59
-- Always write the final code yourself
-- Cite any websites you referenced
-- Use the PEP-8 checker for full style points:
https://pypi.python.org/pypi/pep8
"""


def music_recs(artist):
    '''Search ifyoudig.net for the specified artist. Do some searches yourself
    to see how this works. You only have to support artists that appear here:
    http://ifyoudig.net/autocomplete.php.
    If any other input is given, throw an exception with the message:
    "Artist not found.".

    You should convert the artist name to the specified hyphenated
    slug, and then query the appropriate url. Return a list of the top
    10 recommended artists.

    Note that most of the artist names are links, but some aren't;
    e.g. see http://ifyoudig.net/laura-marling. You should be able to handle
    either case.

    Assume that the artist will have correct capitalization if a valid artist
    '''
    pass


def xkcd(comic_num):
    ''' Pull up the comic_num-th xkcd comic on http://xkcd.com
         example (http://xkcd.com/1488/)
    Return a dictionary with the keys: 'image', 'comic_title', and 'title_text'
    The values should be:
      'image' -> a relative file path to a downloaded image of the comic
                 store the file in a directory called imgs
                 Do not assume that the directory already exists
      'comic_title' -> The string that is the title of the comic
      'title_text' -> The string that is the title attribute of the image
    If comic_num is beyond the range of valid comics (1-1490 when I wrote this)
      Then raise a ValueError with the message:
        'comic_num is an invalid xkcd comic. try [1-{last_comic_num}]'
      You should raise this error only for requests.codes.not_found
    '''
    pass


def main():
    pass


if __name__ == "__main__":
    main()
